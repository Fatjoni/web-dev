import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { LoginComponent } from '../atomic-design/organisms/login/login.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';
import { NewsFeedComponent } from '../atomic-design/organisms/news-feed/news-feed.component';
import { ToolbarComponennt } from '../atomic-design/molecules/toolbar.component/toolbar.component';
import { AskTellNoteComponent } from '../atomic-design/molecules/ask-tell-note/ask-tell-note.component';
import { AskComponent } from '../atomic-design/atoms/ask/ask.component';
import { AvatarComponent } from '../atomic-design/atoms/avatar/avatar.component';
import { TellComponent } from '../atomic-design/atoms/tell/tell.component';
import { NoteComponent } from '../atomic-design/atoms/note/note.component';
import { AllPostDiscoverComponent } from '../atomic-design/molecules/all-post-discover/all-post-discover.component';
import { AllPostsComponent } from '../atomic-design/molecules/all-posts/all-posts.component';
import { AllPostsTitleComponent } from '../atomic-design/atoms/all-posts-title/all-posts-title.component';
import { DiscoverPeopleComponent } from '../atomic-design/molecules/discover-people/discover-people.component';
import { DiscoverOnePersonComponent } from '../atomic-design/atoms/discover-one-person/discover-one-person.component';
import { SuggestedInterstsComponent } from '../atomic-design/molecules/suggested-intersts/suggested-intersts.component';
import { SingleSuggestedInterestComponent } from '../atomic-design/atoms/single-suggested-interest/single-suggested-interest.component';

@NgModule({
  declarations: [
    LoginComponent,
    NewsFeedComponent,
    ToolbarComponennt,
    AskTellNoteComponent,
    AskComponent,
    AvatarComponent,
    TellComponent,
    NoteComponent,
    AllPostDiscoverComponent,
    AllPostsComponent,
    AllPostsTitleComponent,
    DiscoverPeopleComponent,
    DiscoverOnePersonComponent,
    SuggestedInterstsComponent,
    SingleSuggestedInterestComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule,
    RouterModule
  ],
  exports: [
    LoginComponent,
    NewsFeedComponent,
    ToolbarComponennt
  ],
  providers: [],
})
export class Sharedmodule { }
