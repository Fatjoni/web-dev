export interface MetaData {
    id: string,
    url: string,
    meta_image: string,
    meta_title: string,
    meta_description: string
}