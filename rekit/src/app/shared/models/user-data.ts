import { PhoneData } from "./phone-data";

export interface UserData {
    id: number,
    first_name: string,
    last_name: string,
    email: string,
    birthday: string,
    postcode: string,
    job_title: string,
    gender: string,
    description: string,
    status: number,
    file_id: number,
    date_created: Date,
    url: string,
    profile_image: string,
    type: string,
    phone: PhoneData
}