export interface Place {
    id: string,
    place_name: string,
    place_address: string,
    place_url: string,
    place_logo: string,
    place_latitude: string,
    place_longitude: string,
    place_type: string,
    place_opens: string
}