import { Category } from "./category";
import { UserData } from "./user-data";
import { Place } from "./place";
import { MetaData } from "./meta-data";

export interface AllPostsData {
    id: number,
    type: number,
    visibility: number,
    category: Category,
    title: string,
    content: string,
    rate: number,
    urgency: boolean,
    can_forward: boolean,
    is_forwarding: boolean,
    my_stuff: boolean,
    create: string,
    owner: UserData,
    photos: Array<string>,  
    title_question: string,
    place: Place,
    metaData: MetaData,
    replies: number,
    likes: number,
    saves: number,
    shares: number,
    is_liked: boolean,
    is_saved: boolean,
    is_favorite: boolean,
    notification_turn_off: boolean
}