import { UserData } from "./user-data";

export interface UserModel {
    access_token: string,
    user: UserData,
    new_user: boolean
}