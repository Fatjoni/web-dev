import { UserData } from "./user-data";

export interface UserSession {
    access_token: string,
    new_user: boolean
}