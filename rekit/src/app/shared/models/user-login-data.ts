export interface UserLoginData {
    email: string;
}

export const mapToUserLogindata = function(response: UserLoginData) {
    return { email: response.email };
}