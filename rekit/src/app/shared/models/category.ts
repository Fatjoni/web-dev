export interface Category {
    id: number,
    name: string,
    default_id: string,
    is_default: boolean,
    icon: string,
    last_seen: string
}