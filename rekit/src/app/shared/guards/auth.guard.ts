import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { Store, select } from '@ngrx/store';

import { Observable, of } from 'rxjs';
// import 'rxjs/add/operator/take';
// import 'rxjs/add/operator/switchMap';
// import 'rxjs/add/operator/do';
// import 'rxjs/add/operator/catch';
// import { of } from 'rxjs/observable/of';

// import { CoursesState, getCoursesState } from '../store/reducers/';
// import * as Courses from '../store/actions/courses.actions';
import { switchMap, catchError, filter, tap, take } from 'rxjs/operators';
import { getLoginState } from '../../ngrx/reducers';
import * as login from '../../ngrx/actions/auth.actions';
import * as fromRoot from '../../ngrx/reducers'
import { UserModel } from '../models/user-model';
@Injectable()
export class AuthGuard {
  constructor(private store: Store<fromRoot.State>) {}

//   getFromStoreOrAPI(): Observable<any> {
//     return this.store.pipe(
//         tap((data: UserModel) => {
//         if (!data !== undefined) {
//             this.store.dispatch(new login.LoginSuccessAction(data));
//         }
//         }),
//         filter((data: UserModel) => data.user.email !== ''),
//         take(1)
//     )

//   }

//   canActivate(): Observable<boolean> {
//     return this.getFromStoreOrAPI()
//     .pipe(
//         switchMap(() => of(true)),
//         catchError(() => of(false))
//     )
  // }
}