import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {MatInputModule} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatRadioModule} from '@angular/material/radio';
import {MatListModule} from '@angular/material/list';
import { MatFormFieldModule, MatOptionModule, MatSelectModule, 
  MatCardModule, MatTableModule, MatDividerModule, MatSnackBarModule } from '@angular/material';
import {MatTabsModule} from '@angular/material/tabs';

@NgModule({
  exports: [
    // Export here material module that you need :)
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatSidenavModule,
    MatRadioModule,
    MatListModule,
    MatFormFieldModule, 
    MatOptionModule, 
    MatSelectModule, 
    MatIconModule, 
    MatCardModule, 
    MatTableModule, 
    MatDividerModule, 
    MatSnackBarModule,
    MatTabsModule
  ]
})
export class MaterialModule {}
