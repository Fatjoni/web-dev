import { Action } from "@ngrx/store";
import { UserSession } from "src/app/shared/models/user-session";
import { UserModel } from "src/app/shared/models/user-model";
import { UserData } from "src/app/shared/models/user-data";
import { UserLoginData } from "src/app/shared/models/user-login-data";

export const LOGIN = '[Login] Login';
export const LOGIN_SUCCESS = '[Login] Login Success';
export const LOGIN_FAIL = '[Login] Login Fail';

export class LoginAction implements Action {
    readonly type = LOGIN;

    constructor(public payload: UserLoginData) {}
}

export class LoginSuccessAction implements Action {
    readonly type = LOGIN_SUCCESS;
    
    constructor(public payload: UserModel) {}
}

export class LoginFailAction implements Action {
    readonly type = LOGIN_FAIL;

    constructor( public payload: string) {}
}

export type Action =
    | LoginAction
    | LoginSuccessAction
    | LoginFailAction;