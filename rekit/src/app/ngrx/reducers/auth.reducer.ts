import { UserSession } from "src/app/shared/models/user-session";
import * as auth from "../actions/auth.actions";
import { UserModel } from "src/app/shared/models/user-model";

export interface State {
    loading: boolean,
    loggedin: boolean,
    hasError: boolean,
    userData: UserModel,
    errorText: string
};

export const emptySession: UserSession = {
    access_token: null,
    new_user: false
}

export const emptyUserData: UserModel = {
    access_token: '',
    user: {
        id: null,
        first_name: null,
        last_name: null,
        email: null,
        birthday: null,
        postcode: null,
        job_title: null,
        gender: null,
        description: null,
        status: null,
        file_id: null,
        date_created: null,
        url: null,
        profile_image: null,
        type: null,
        phone: {
            id: null,
            number: null
        }
    },
    new_user: false

};

export const initialAuthState: State = {
    loading: false,
    loggedin: false,
    hasError: false,
    userData: emptyUserData,
    errorText: ''
}

export function reducer (state: State, action: auth.Action): State {
    switch (action.type) {
        case auth.LOGIN: {
            return { ...state, loading: true, hasError: false }
        }

        case auth.LOGIN_SUCCESS: {
            return { ...state, loading: false, hasError: false, loggedin: true, userData: { ...action.payload} }
        }

        case auth.LOGIN_FAIL: {
            return { ...state, loading: false, hasError: true, errorText: action.payload }
        }

        default: {
            return state;
        }
    }
}
export const getAuthState = (state: State) => state;
export const getLoggedIn = (state: State) => state.loggedin;
export const getAvatar = (state: State) => state.userData.user.profile_image