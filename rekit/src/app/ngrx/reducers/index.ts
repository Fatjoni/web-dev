import * as fromAuth from "./auth.reducer"
import { createSelector } from "@ngrx/store";

export interface State {
    auth: fromAuth.State,
}

export const initialState = {
    auth: fromAuth.initialAuthState,
}

export const reducer = {
    auth: fromAuth.reducer,
}


// Auth
const getAuthState = (state: State) => state.auth;
export const getLoggedIn = createSelector(getAuthState, fromAuth.getLoggedIn);
export const getLoginState = createSelector(getAuthState, fromAuth.getAuthState);
// export const getUserAvatar = createSelector(getAuthState, fromAuth.)