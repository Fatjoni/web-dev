import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { UserModel } from "src/app/shared/models/user-model";
import { map } from "rxjs/operators";

const headers = new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
});

@Injectable()
export class AuthService {
    private userLoginUrl = 'http://api.dev.teepee.me/auth/authfb';
    getCommonHeaders(){
        let headers = new HttpHeaders();
        headers.append('Content-Type','application/json');
        return headers;
      }

    constructor(private http: HttpClient) {}

    public login(email: string): Observable<UserModel> {
        const body = { email: email };
        return this.http.post<UserModel>(this.userLoginUrl, body, {headers: this.getCommonHeaders()}).pipe(
            map((r: {data: UserModel}) => {
                return r.data;
            })
        )
    }

    public logout(): Observable<boolean> {
        return this.http.delete(this.userLoginUrl, {}).pipe(map(() => true))
    }
}