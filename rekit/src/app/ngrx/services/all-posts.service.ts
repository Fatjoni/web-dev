import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { AllPostsData } from "src/app/shared/models/all-posts-data";
import { map, catchError } from "rxjs/operators";

@Injectable()
export class AllPostsService {
    private allPostUrl = 'http://api.dev.teepee.me/post/follow_public_feed/1/2';
    getHeaders(){
        let headers = new HttpHeaders();
        headers.append('Authorization','y-6eWI2r4kgOxHzF6iX2fyY1Gh1FllFDUltF2ibYM_Iu8aIvnrLQcDcHtz-GV5GIJJSKv4E4mAg_jNGY');
        headers.append('Content-Type','application/json');
        return headers;
      }


    constructor(private http: HttpClient) {}

    public getAllPosts(nrOfPosts: number, nrOfPage: number): Observable<AllPostsData[]>{
        return this.http.get<AllPostsData[]>(this.allPostUrl, { headers: this.getHeaders() }).pipe(
            map((r: {data: AllPostsData[]}) => {
                console.log(r.data)
                return r.data;
            })
        )
    }
}