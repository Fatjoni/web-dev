import { Actions, Effect, ofType } from "@ngrx/effects";
import * as auth from "../actions/auth.actions";
import { catchError, map, switchMap, exhaustMap, tap } from "rxjs/operators"
import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import * as fromRoot from "../reducers";
import { AuthService } from "../services/auth.service";
import { of } from "rxjs";
import { Router, ActivatedRoute } from "@angular/router";
import { UserModel } from "src/app/shared/models/user-model";
import { UserLoginData } from "src/app/shared/models/user-login-data";

@Injectable()
export class AuthEffects {

  @Effect()
  login$ = this.actions$.pipe(
    ofType<auth.LoginAction>(auth.LOGIN),
    map(action => action.payload),
    exhaustMap((test: UserLoginData) =>
      this.authService.login(test.email).pipe(
        tap(user => {
          // if (user && user.token) {
          //   this.cookieService.set('token', user.token)
          // }
        }),
        map((user: UserModel) => new auth.LoginSuccessAction( user )),
        catchError(error => of(new auth.LoginFailAction(error)))
      )
    )
  );

  @Effect()
  loginSuccessful$ = this.actions$.pipe(
    ofType<auth.LoginSuccessAction>(auth.LOGIN_SUCCESS),
    switchMap((action: auth.LoginSuccessAction) => {
      localStorage.setItem('access_token', JSON.stringify(action.payload.access_token));
      localStorage.setItem('data', JSON.stringify(action.payload))
        this.router.navigateByUrl('news-feed')

      return of();
    }),
  );

  @Effect()
  loginFail$ = this.actions$.pipe(
    ofType<auth.LoginFailAction>(auth.LOGIN_FAIL),
    switchMap((action: auth.LoginFailAction) => {
      this.store.dispatch(new auth.LoginFailAction(action.payload));
      // this.removeSessionFromStorage();

      return of();
    }),
  );


    constructor(private actions$: Actions,
      private store: Store<fromRoot.State>,
      private authService: AuthService,
      private router: Router,
      private route: ActivatedRoute) {}
}