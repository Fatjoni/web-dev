import { Component, OnInit, Input } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'rekit-all-posts',
  templateUrl: './all-posts.component.html',
  styleUrls: ['./all-posts.component.scss']
})
export class AllPostsComponent implements OnInit {
  @Input() avatarImageSrc: string;
  public avatarWidth: string;
  public avatarHeight: string;
  public numberOfLikes: number = 26;
  public numberOfComments: number = 28;
  public numberOfBookmarked: number = 2;
  public numberOfShares: number = 5;
  public allPostsUserName = 'Pamela Van Robertson'; // INFO to be extracted as Input when we get the call
  public allPostsUserLocation = 'London, UK'; // INFO to be extracted as Input when we get the call
  public allPostsUserShowing = 'Lodon'; // INFO to be extracted as Input when we get the call

  public status = 'Following'; // INFO to be extracted as Input when we get the call
  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {
    this.avatarWidth = '68';
    this.avatarHeight = '68';
    matIconRegistry.addSvgIcon(
      'expand-more',
      domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/expand_more.svg'));
      matIconRegistry.addSvgIcon(
        'notification',
        domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/notification.svg'));
      matIconRegistry.addSvgIcon(
        'user',
        domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/user-icon.svg'));
      matIconRegistry.addSvgIcon(
        'heart',
        domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/heart-icon.svg'));
      matIconRegistry.addSvgIcon(
        'bookmark',
        domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/icon-bookmark.svg'));
      matIconRegistry.addSvgIcon(
        'share',
        domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/icon-share.svg'));
   }

  ngOnInit() {
  }

}
