import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'rekit-all-post-discover',
  templateUrl: './all-post-discover.component.html',
  styleUrls: ['./all-post-discover.component.scss']
})
export class AllPostDiscoverComponent implements OnInit {
  @Input() avatarImageSrc: string;
  constructor() { }

  ngOnInit() {
  }

}
