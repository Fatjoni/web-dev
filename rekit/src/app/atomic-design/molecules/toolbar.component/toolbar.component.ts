import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { MatIconRegistry } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush

  })
export class ToolbarComponennt {
  @Input() avatarImageSrc: string;
  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {
    matIconRegistry.addSvgIcon(
      'notification',
      domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/notification.svg'));
    matIconRegistry.addSvgIcon(
      'favorite-folder',
      domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/favorite-folder.svg'));
    matIconRegistry.addSvgIcon(
      'invite-friends',
      domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/invite-friends.svg'));
    matIconRegistry.addSvgIcon(
      'logo-rekit',
      domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/logo-rekit.svg'));
  }
    
}