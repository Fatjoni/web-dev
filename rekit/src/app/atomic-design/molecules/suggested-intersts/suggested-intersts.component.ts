import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'rekit-suggested-intersts',
  templateUrl: './suggested-intersts.component.html',
  styleUrls: ['./suggested-intersts.component.scss']
})
export class SuggestedInterstsComponent implements OnInit {

  @Input() avatarImageSrc: string;

  public suggestedInterests = [{
    suggestedInterestName: 'Julienne Laux',
  },
  {
    suggestedInterestName: 'Julienne Laux',
  },
  {
    suggestedInterestName: 'Julienne Laux',
  },
  {
    suggestedInterestName: 'Julienne Laux',
  },
  {
    suggestedInterestName: 'Julienne Laux',
  },
  {
    suggestedInterestName: 'Julienne Laux',
  },
];
  constructor() { }

  ngOnInit() {
  }

}
