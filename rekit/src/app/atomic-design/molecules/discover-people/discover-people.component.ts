import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'rekit-discover-people',
  templateUrl: './discover-people.component.html',
  styleUrls: ['./discover-people.component.scss']
})
export class DiscoverPeopleComponent implements OnInit {
  @Input() avatarImageSrc: string;

  public discoverPerson = [{
    personName: 'Julienne Laux',
    personBio: 'Designer by day, developer by night'
  },
  {
    personName: 'Julienne Laux',
    personBio: 'Designer by day, developer by night'
  },
  {
    personName: 'Julienne Laux',
    personBio: 'Designer by day, developer by night'
  },
  {
    personName: 'Julienne Laux',
    personBio: 'Designer by day, developer by night'
  },
  {
    personName: 'Julienne Laux',
    personBio: 'Designer by day, developer by night'
  },
  {
    personName: 'Julienne Laux',
    personBio: 'Designer by day, developer by night'
  },
];
  constructor() { }

  ngOnInit() {
  }

}
