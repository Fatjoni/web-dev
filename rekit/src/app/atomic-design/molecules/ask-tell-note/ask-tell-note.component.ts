import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-ask-tell-note',
  templateUrl: './ask-tell-note.component.html',
  styleUrls: ['./ask-tell-note.component.scss']
})
export class AskTellNoteComponent {
  @Input() avatarImageSrc: string;
  constructor() { }

}
