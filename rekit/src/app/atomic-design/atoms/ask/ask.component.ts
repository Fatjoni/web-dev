import { Component, ViewChild, ElementRef, OnChanges, Input, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-ask',
  templateUrl: './ask.component.html',
  styleUrls: ['./ask.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AskComponent implements OnChanges {
  @Input() avatarImageSrc: string;
  @ViewChild('textarea') textarea: ElementRef;
  public textAreaForm: FormGroup;
  public editMode: boolean;
  constructor(private formBuilder: FormBuilder,) { 
    this.textAreaForm = this.formBuilder.group({
      email: ['', Validators.required],
    });
  }

  ngOnChanges() {
    if (!this.editMode) {
      // this.textarea.nativeElement.focus();
    }
  }

  public toggleEditMode() {
    this.editMode = true;
  }

  public cancelEditMode() {
    this.editMode = false;
  }
}
