import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'rekit-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent {
  public textAreaForm: FormGroup;
  constructor(private formBuilder: FormBuilder) { 
    this.textAreaForm = this.formBuilder.group({
      email: ['', Validators.required],
    });
  }
}
