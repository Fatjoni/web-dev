import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent {
  @Input() avatarWidth = 43;
  @Input() avatarHeight = 43;
  @Input() avatarImageSrc: string;
  constructor() {}
}
