import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rekit-discover-one-person',
  templateUrl: './discover-one-person.component.html',
  styleUrls: ['./discover-one-person.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DiscoverOnePersonComponent implements OnInit {
  @Input() avatarImageSrc: string;
  @Input() discoverPersonName: string;
  @Input() discoverPersonBio: string;
  public avatarWidth = '62';
  public avatarHeight = '62';

  constructor() { }

  ngOnInit() {
  }

}
