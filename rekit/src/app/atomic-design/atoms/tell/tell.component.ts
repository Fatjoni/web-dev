import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'rekit-tell',
  templateUrl: './tell.component.html',
  styleUrls: ['./tell.component.scss']
})
export class TellComponent {
  public textAreaForm: FormGroup;
  constructor(private formBuilder: FormBuilder,) { 
    this.textAreaForm = this.formBuilder.group({
      email: ['', Validators.required],
    });
  }

}
