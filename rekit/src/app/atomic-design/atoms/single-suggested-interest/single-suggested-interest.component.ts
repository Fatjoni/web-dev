import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rekit-single-suggested-interest',
  templateUrl: './single-suggested-interest.component.html',
  styleUrls: ['./single-suggested-interest.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SingleSuggestedInterestComponent implements OnInit {
  @Input() avatarImageSrc: string;
  @Input() suggestedInterestName: string;
  public avatarWidth = '62';
  public avatarHeight = '62';
  constructor() { }

  ngOnInit() {
  }

}
