import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'rekit-all-posts-title',
  templateUrl: './all-posts-title.component.html',
  styleUrls: ['./all-posts-title.component.scss']
})
export class AllPostsTitleComponent implements OnInit {
  @Input() allPostsUserName: string;
  @Input() allPostsUserLocation: string;
  @Input() allPostsUserShowing: string;
  constructor() { }

  ngOnInit() {
  }

}
