import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { UserModel } from "src/app/shared/models/user-model";
import { Subscription, Observable, of } from "rxjs";
import { Store, select } from "@ngrx/store";
import * as fromRoot from "../../../ngrx/reducers"
import * as auth from "../../../ngrx/actions/auth.actions"
import { AllPostsService } from "src/app/ngrx/services/all-posts.service";
import { catchError, map } from "rxjs/operators";
import { AllPostsData } from "src/app/shared/models/all-posts-data";
@Component({
    selector: 'app-news-feed',
    templateUrl: './news-feed.component.html',
    styleUrls: ['./news-feed.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewsFeedComponent implements OnInit {
    public user: UserModel;
    private isLoggedIn: boolean;
    public avatarImageSrc: string;
    public subscription: Subscription[] = [];
    constructor(private store: Store<fromRoot.State>, private allPosts: AllPostsService) {
        this.subscription.push(this.store.pipe(select(fromRoot.getLoggedIn)).subscribe(s => this.isLoggedIn = s))
        this.subscription.push(this.store.pipe(select(fromRoot.getLoginState)).subscribe((l: any) => {
            if (l && l.userData.user.profile_image) {
              this.avatarImageSrc = 'http://static.dev.teepee.me' + l.userData.user.profile_image
            }
        }));
    }

    ngOnInit() {
        // this.allPosts.getAllPosts(1, 1)
    }

}