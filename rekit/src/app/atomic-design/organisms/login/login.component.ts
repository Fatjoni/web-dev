import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { UserModel } from 'src/app/shared/models/user-model';
import * as fromRoot from '../../../ngrx/reducers';
import { Store } from '@ngrx/store';
import { LoginAction } from 'src/app/ngrx/actions/auth.actions';
import { AllPostsService } from 'src/app/ngrx/services/all-posts.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class LoginComponent {
  public loginForm: FormGroup;
  public test: UserModel;
  private allPosts: AllPostsService
  constructor(
    private formBuilder: FormBuilder,
    private store: Store<fromRoot.State>) {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
    });
  }

  onLogin() {
    this.store.dispatch(new LoginAction({email: this.loginForm.controls.email.value}));
  }

}
