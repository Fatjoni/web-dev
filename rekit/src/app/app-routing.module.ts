import {RouterModule, Routes} from '@angular/router';
import {NgModule, Component} from '@angular/core';
import {LoginComponent} from './atomic-design/organisms/login/login.component';
import { NewsFeedComponent } from './atomic-design/organisms/news-feed/news-feed.component';
import { AuthGuard } from './shared/guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'news-feed', component: NewsFeedComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
